import pytest
from utils.cpf_validator_util import CpfValidator

def test_valid_cpf_without_punctuation():
    assert CpfValidator.validate("12345678909") is True

def test_cpf_with_invalid_length():
    assert CpfValidator.validate("123.456.789") is False

def test_valid_cpf_with_different_verification_digits():
    assert CpfValidator.validate("111.222.333-96") is True

def test_cpf_with_repeated_digits():
    assert CpfValidator.validate("111.222.333-44") is False

def test_cpf_with_invalid_characters():
    assert CpfValidator.validate("12A.456.789-09") is False

