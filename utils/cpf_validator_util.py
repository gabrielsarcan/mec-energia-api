class CpfValidator:
    @staticmethod
    def validate(cpf):
        # Remover caracteres não numéricos
        cpf = ''.join(filter(str.isdigit, cpf))
        
        # Verificar se o CPF tem 11 dígitos
        if len(cpf) != 11:
            return False

        # Calcular dígito verificador 1
        total = sum(int(cpf[i]) * (10 - i) for i in range(9))
        digit1 = 11 - (total % 11)
        digit1 = digit1 if digit1 < 10 else 0

        # Calcular dígito verificador 2
        total = sum(int(cpf[i]) * (11 - i) for i in range(10))
        digit2 = 11 - (total % 11)
        digit2 = digit2 if digit2 < 10 else 0

        # Verificar se os dígitos verificadores são iguais aos fornecidos
        return cpf[-2:] == f"{digit1}{digit2}"

